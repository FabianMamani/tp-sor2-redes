#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/ipv4-global-routing-helper.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("DumbbellExample");

int main(int argc, char* argv[]) {
    CommandLine cmd(__FILE__); ;
    cmd.Parse(argc, argv);

    // Creación de dos nodos
    NodeContainer nodes;
    nodes.Create(2);

    // Configuración de los enlaces punto a punto
    PointToPointHelper p2p;
    p2p.SetDeviceAttribute("DataRate", StringValue("5Mbps"));
    p2p.SetChannelAttribute("Delay", StringValue("2ms"));

    // Instalación de los dispositivos de red
    NetDeviceContainer devices;
    devices = p2p.Install(nodes);

    // Configuración de las pilas de protocolos de Internet
    InternetStackHelper stack;
    stack.Install(nodes);

    // Asignación de direcciones IP
    Ipv4AddressHelper address;
    address.SetBase("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer interfaces = address.Assign(devices);

    // Creación de emisores/receptores TCP
    for (int i = 0; i < 2; i++) {
        // Emisor/receptor TCP
        uint16_t port = i + 5000;
        OnOffHelper clientHelper("ns3::TcpSocketFactory", Address(InetSocketAddress(interfaces.GetAddress(1), port)));
        clientHelper.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        clientHelper.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));

        // Instalación de la aplicación cliente en el nodo 0
        ApplicationContainer clientApps = clientHelper.Install(nodes.Get(0));
        clientApps.Start(Seconds(1.0));
        clientApps.Stop(Seconds(10.0));

        // Servidor TCP
        PacketSinkHelper sinkHelper("ns3::TcpSocketFactory", Address(InetSocketAddress(interfaces.GetAddress(1), port)));
        ApplicationContainer serverApp = sinkHelper.Install(nodes.Get(1));
        serverApp.Start(Seconds(1.0));
        serverApp.Stop(Seconds(10.0));
    }

    // Creación de emisor/receptor UDP
    uint16_t udpPort = 6000;
    OnOffHelper udpClientHelper("ns3::UdpSocketFactory", Address(InetSocketAddress(interfaces.GetAddress(1), udpPort)));
    udpClientHelper.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
    udpClientHelper.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));

    // Instalación de la aplicación cliente UDP en el nodo 0
    ApplicationContainer udpClientApp = udpClientHelper.Install(nodes.Get(0));
    udpClientApp.Start(Seconds(2.0));
    udpClientApp.Stop(Seconds(10.0));

    // Servidor UDP
    UdpServerHelper udpServer(udpPort);
    ApplicationContainer udpServerApp = udpServer.Install(nodes.Get(1));
    udpServerApp.Start(Seconds(2.0));
    udpServerApp.Stop(Seconds(10.0));

    // Habilitar la captura en pcap para el enlace punto a punto
    p2p.EnablePcapAll("dumbell");


    // Ejecución de la simulación
    Simulator::Run();
    Simulator::Destroy();

    return 0;
}
